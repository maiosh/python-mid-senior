# Testowe zadanie

## Opis zadania

Stworzyć API, które zapewni funkcjonalność URL Shortener (coś ala https://goo.gl).

## Wymagania biznesowe

- konta użytkowników,
- użytkownik automatycznie jest wylogowywany po 15 minutach nieaktywności,
- towrzenie publicznych linków skróconych (widoczne w portalu bez zalogowania),
- tworzenie prywatnych linków skróconych (widoczne jedynie dla danego użytkownika),
- prywatny link nadal może być otwarty przez każdą osobę (nawet nie będącą użytkownikiem), jedynie nie jest widoczny na liście linków skróconych,
- linki skrócone mogą mieć ustawiane autowygaśnięcie: (1h, 12h, 24h, 7 dni)
- 1 statystyka: liczba prywatnych i publicznych linków skróconych w danym miesiącu,

## Lista endpointów API

- logowanie użytkownika,
- wylogowanie użytkownika,
- publiczne pobieranie listy utworzonych skróconych linków (tylko publiczne linki),
- pobieranie listy utworzonych skróconych linków (dla zalogowanego użytkownika) (prywatne i publiczne linki),
- stworzenie linku skróconego (prywatnego i publicznego) z możliwością podania czasu autowygaśnięcia,
- zmiana typu linku (publiczy|prywatny) oraz zmiana czasu ważności,
- usuwanie linku skróconego

## Wymagania techniczne

- Tornado (lub inny asynchroniczny framework)
- MongoDB (pymotor)
- użycie asynchronicznego api w Python (coroutines lub asyncio)
- unittest i integration tests,
- ładowanie konfiguracji zależnej od środowiska (np dev, test, produkcja)